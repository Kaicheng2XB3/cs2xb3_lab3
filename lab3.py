import random
import math
import timeit
def my_quicksort(L):
    copy = quicksort_copy(L)
    for i in range(len(L)):
        L[i] = copy[i]


def quicksort_copy(L):
    if len(L) < 2:
        return L
    pivot = L[0]
    left, right = [], []
    for num in L[1:]:
        if num < pivot:
            left.append(num)
        else:
            right.append(num)
    return quicksort_copy(left) + [pivot] + quicksort_copy(right)


def create_random_list(n):
    L = []
    for _ in range(n):
        L.append(random.randint(1,n))
    return L


def create_near_sorted_list(n, factor):
    L = create_random_list(n)
    L.sort()
    for _ in range(math.ceil(n*factor)):
        index1 = random.randint(0, n-1)
        index2 = random.randint(0, n-1)
        L[index1], L[index2] = L[index2], L[index1]
    return L

def quicksort_inplace(L):
    for i in range(len(L)):
        mindex = find_min_index(L,i)
        L[i], L[mindex] = L[mindex], L[i]


def find_min_index(L,i):
    mindex = i
    for j in range(i+1,len(L)):
        if L[j] < L[mindex]:
            mindex = j
    return mindex

def timetest(runs,length,sort):
    total = 0
    for _ in range(runs):
        L = create_random_list(length)
        start = timeit.default_timer()
        sort(L)
        end = timeit.default_timer()
        total += end - start
    return total/runs

#L = create_random_list(10)

#for i in range(100,2000,100):
    #print(i, timetest(10,5,quicksort_inplace))


def dual_pivot_quicksort(L):
    if len(L) < 2:
        return L
    elif len(L) == 2:
        return sorted(L)
    pivot1, pivot2 = sorted([L[0], L[1]])
    part1, part2, part3 = [], [], []
    for num in L[2:]:
        if num < pivot1:
            part1.append(num)
        elif pivot1 <= num < pivot2:
            part2.append(num)
        else:
            part3.append(num)
    return dual_pivot_quicksort(part1) + [pivot1] + dual_pivot_quicksort(part2) \
        + [pivot2] + dual_pivot_quicksort(part3)

#L = [6,5,4,3,2,1]
#print(dual_pivot_quicksort(L))

def tri_pivot_quicksort(L):
    if len(L) < 2:
        return L
    elif len(L) == 2:
        return sorted(L)
    pivot1, pivot2, pivot3 = sorted([L[0], L[1], L[2]])
    part1, part2, part3, part4 = [], [], [], []
    for num in L[3:]:
        if num < pivot1:
            part1.append(num)
        elif pivot1 <= num < pivot2:
            part2.append(num)
        elif pivot2 <= num < pivot3:
            part3.append(num)
        else:
            part4.append(num)
    return tri_pivot_quicksort(part1) + [pivot1] + tri_pivot_quicksort(part2) + [pivot2] + \
        tri_pivot_quicksort(part3) + [pivot3] + tri_pivot_quicksort(part4)

#L = [6,5,4,3,2,1]
#print(tri_pivot_quicksort(L))

def quad_pivot_quicksort(L):
    if len(L) <= 1:
        return L
    elif len(L) == 2 or len(L) == 3:
        return sorted(L)
    pivot1, pivot2, pivot3, pivot4 = sorted([L[0], L[1], L[2], L[3]])
    part1, part2, part3, part4, part5 = [], [], [], [], []
    for num in L[4:]:
        if num < pivot1:
            part1.append(num)
        elif pivot1 <= num < pivot2:
            part2.append(num)
        elif pivot2 <= num < pivot3:
            part3.append(num)
        elif pivot3 <= num < pivot4:
            part4.append(num)
        else:
            part5.append(num)
    return quad_pivot_quicksort(part1) + [pivot1] + quad_pivot_quicksort(part2) + [pivot2] + \
        quad_pivot_quicksort(part3) + [pivot3] + quad_pivot_quicksort(part4) + [pivot4] + quad_pivot_quicksort(part5)

#L = [6,5,4,3,2,1]
#print(quad_pivot_quicksort(L))