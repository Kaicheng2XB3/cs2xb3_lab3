import lab3
def insert_at(iList,iElement,iRange):
    iList.append(iList[iElement])
    if (iRange > iElement):
        for i in range(iElement + 1,iRange + 1):
            iList[i-1] = iList[i]
        iList[iRange] = iList[-1]
    elif (iRange < iElement):
        for i in range(iElement,iRange,-1):
            iList[i] = iList[i - 1]
        iList[iRange] = iList[-1]
    else:
        pass
    iList.pop(-1)
    return iList

def quicksort_inplace(L):
    if len(L) < 2:
        return L
    pivot = L[0]
    pivot_index = 0
    num = 1
    length_index = 0
    while (num < len(L)):
        if (length_index + pivot_index < len(L)):
            if L[num] < pivot:
                insert_at(L,num,0)
                pivot_index += 1
                num += 1
            else:
                insert_at(L,num,len(L))
                length_index += 1
        else:
            break
    return quicksort_inplace(L[:pivot_index]) + [pivot] + quicksort_inplace(L[pivot_index + 1:])

def dual_pivot_quicksort(L):

    # The first part: len(L) <= 2
    if len(L) < 2:
        return L
    elif len(L) == 2:
        return [min(L[0],L[1]),max(L[0],L[1])]

    # The main part.
    else:
        pivot1 = min(L[0],L[1])
        pivot2 = max(L[0],L[1])
        left, mid, right = [], [], []
        for num in L[2:]:
            if (num < pivot1):
                left.append(num)
            elif (num < pivot2):
                mid.append(num)
            else:
                right.append(num)
        return dual_pivot_quicksort(left) + [pivot1] + dual_pivot_quicksort(mid) + [pivot2] + dual_pivot_quicksort(right)
            
def tri_pivot_quicksort(L):
    if len(L) < 2:
        return L
    elif len(L) == 2:
        return sorted(L)
    pivot1, pivot2, pivot3 = sorted([L[0], L[1], L[2]])
    part1, part2, part3, part4 = [], [], [], []
    for num in L[3:]:
        if num < pivot1:
            part1.append(num)
        elif pivot1 <= num < pivot2:
            part2.append(num)
        elif pivot2 <= num < pivot3:
            part3.append(num)
        else:
            part4.append(num)
    return tri_pivot_quicksort(part1) + [pivot1] + tri_pivot_quicksort(part2) + [pivot2] + \
        tri_pivot_quicksort(part3) + [pivot3] + tri_pivot_quicksort(part4)

#L = [6,5,4,3,2,1]
#print(tri_pivot_quicksort(L))

def quad_pivot_quicksort(L):
    if len(L) <= 1:
        return L
    elif len(L) == 2 or len(L) == 3:
        return sorted(L)
    pivot1, pivot2, pivot3, pivot4 = sorted([L[0], L[1], L[2], L[3]])
    part1, part2, part3, part4, part5 = [], [], [], [], []
    for num in L[4:]:
        if num < pivot1:
            part1.append(num)
        elif pivot1 <= num < pivot2:
            part2.append(num)
        elif pivot2 <= num < pivot3:
            part3.append(num)
        elif pivot3 <= num < pivot4:
            part4.append(num)
        else:
            part5.append(num)
    return quad_pivot_quicksort(part1) + [pivot1] + quad_pivot_quicksort(part2) + [pivot2] + \
        quad_pivot_quicksort(part3) + [pivot3] + quad_pivot_quicksort(part4) + [pivot4] + quad_pivot_quicksort(part5)
